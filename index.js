import React from 'react';
import {AppRegistry, StyleSheet, Text, View, Button} from 'react-native';
import {name as appName} from './app.json';
import ReactNativeBrownfield from '@callstack/react-native-brownfield';
import EventBridge from 'react-native-event-bridge';

class HelloWorld extends React.Component {
  _onButtonPress = () => {
    // Post an event to native
    EventBridge.emitEvent(this, 'LearnMore', { 'key': 'value' });

    // Go back to native app
    ReactNativeBrownfield.popToNative(true);
  };

  render() {
    ReactNativeBrownfield.setNativeBackGestureAndButtonEnabled(true);

    return (
      <>
        <View style={styles.container}>
          <Text style={styles.hello}>Hello, World</Text>
        </View>
        <View style={styles.container}>
          <Text style={styles.hello}>Initial Props</Text>
          <Text style={styles.hello}>{this.props ? JSON.stringify(this.props) : 'NADA'}</Text>
        </View>
        <View style={styles.container}>
          <Button onPress={this._onButtonPress} title="Emit Event" />
        </View>
      </>
    );
  }
}
var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  hello: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

AppRegistry.registerComponent(appName, () => HelloWorld);
