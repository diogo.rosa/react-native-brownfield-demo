//
//  SwiftViewController.swift
//  newproject
//
//  Created by Diogo Rosa on 02/10/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit


class SwiftViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationController?.navigationBar.isHidden=false
  }
  
  @IBAction func test(_ sender: Any) {
    
    let rnApp = UserDefaults.standard.string(forKey: "RNApp")
    let rnc = ReactNativeViewController.init(moduleName: rnApp!, andInitialProperties: ["score":"ios"])
    
    self.navigationController?.pushViewController(rnc, animated:true)
  }
  
}

extension UIViewController: MSREventBridgeEventReceiver {
  
  // MARK: Handle events from RN
  
  func onEvent(withName name: String!, info: [AnyHashable : Any]!) {
    print("Received event: \(name) with info \(info)")
  }
  
  func onEvent(withName name: String!, info: [AnyHashable : Any]!, callback: RCTResponseSenderBlock!) {
    print("Received event with callback: \(name) with info \(info)")
    callback([])
  }

}
