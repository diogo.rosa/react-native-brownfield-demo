package com.newproject;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.fragment.app.Fragment;

import com.callstack.reactnativebrownfield.ReactNativeFragment;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;

import javax.annotation.Nullable;

import net.mischneider.MSREventBridgeEventReceiver;
import net.mischneider.MSREventBridgeReceiverCallback;

public class ReactNativeFragmentActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler, MSREventBridgeEventReceiver {
    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_react_native_fragment);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            Bundle bundle = new Bundle();
            bundle.putString("score", "android");

            ReactNativeFragment reactNativeFragment = ReactNativeFragment.createReactNativeFragment(getResources().getString(R.string.rn_name), bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_main, reactNativeFragment)
                    .commit();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        boolean handled = false;
        Fragment activeFragment = getSupportFragmentManager().findFragmentById(R.id.container_main);
        if (activeFragment instanceof ReactNativeFragment) {
            handled = ((ReactNativeFragment)activeFragment).onKeyUp(keyCode, event);
        }
        return handled || super.onKeyUp(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        Fragment activeFragment = getSupportFragmentManager().findFragmentById(R.id.container_main);
        if (activeFragment instanceof ReactNativeFragment) {
            ((ReactNativeFragment)activeFragment).onBackPressed(this);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (getParentActivityIntent() == null) {
                Log.i("onOptionsItemSelected", "You have forgotten to specify the parentActivityName in the AndroidManifest!");
                onBackPressed();
            } else {
                NavUtils.navigateUpFromSameTask(this);
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Handle events
    @Override
    public void onEvent(final String name, final ReadableMap info) {
        Log.d("onEvent", "Received event: ".concat(name).concat(" - ").concat(info.getString("key")));
    }

    // Handle events with callback
    @Override
    public void onEventCallback(final String name, final ReadableMap info, final MSREventBridgeReceiverCallback callback) {
        // Handle event with callback
        // ...
        Log.d("onEventCallback", this.getClass().getName() + ": Received event: ".concat(name));
    }
}
